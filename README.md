## Air Quality Mapper

This is the project code for my project submitted to the "Advanced Wearables with Nordic Semiconductor" contest.

The Air Quality Mapper is a system that measures air pollution and aggregates data on a cloud server for further analysis and visualization. It consist of a portable IoT device with sensors, a custom Android application and a web server.