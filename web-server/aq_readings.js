
var customLabel = {
    restaurant: {
        label: 'R'
    },
    bar: {
        label: 'B'
    },
    fire_fighter: {
        label: 'H'
    },
    fire_position: {
        label: 'F'
    },
    drone_position: {
        label: 'D'
    }
};

var statusIcon = {
    normal: {
        url: 'https://maps.google.com/mapfiles/ms/icons/green-dot.png'
    },
    help: {
        url: 'https://maps.google.com/mapfiles/ms/icons/red-dot.png'
    }
};


// Casa
var initial_main_latitude = -17.419655;
var initial_main_longitude = -66.1645471;

var fire_poly_coord_array_1 = [];
var fire_poly_coord_array_2 = [];
var fire_poly_coord_array_3 = [];

var fireSpot1;
var fireSpot2;
var fireSpot3;
var fire_marker;
var drone_marker;
var fighter_marker;
var fighter_marker_array = [];
var reading_circle_array = [];
var heatmap_array = [];

var map;
var image;

var is_initializing = true;

var task_command = null;
var sel_crew_member_name = "";
var sel_crew_member_idx = null;
var task_cmd_post_data = {};
var task_command = 1;
var sel_sensor_name = "eco2"; // Dafault sensor data to display with heatmap and the sensor graph
var show_markers = false;

var main_latitude = null;
var main_longitude = null;

var weather_get_alternation = true;

var from_date_sel = null;
var to_date_sel = null;

var bounds;
var north_east; // LatLng of the north-east corner
var south_west; // LatLng of the south-west corder
var bounds_dict = {};
var heatmap;

// Icon size and html tags
var icon_size = 25;
var firefighter_icon_tag = "<img src=\"icons/firefighter.png\" width=\"" + icon_size + "\" height=\"" + icon_size + "\">";
var amb_temp_icon_tag = "<img src=\"icons/amb_temp.png\" width=\"" + icon_size + "\" height=\"" + icon_size + "\">";
var body_temp_icon_tag = "<img src=\"icons/body_temp.png\" width=\"" + icon_size + "\" height=\"" + icon_size + "\">";
var heart_rate_icon_tag = "<img src=\"icons/heart_rate.png\" width=\"" + icon_size + "\" height=\"" + icon_size + "\">";
var bar_press_icon_tag = "<img src=\"icons/bar_press.png\" width=\"" + icon_size + "\" height=\"" + icon_size + "\">";
var rel_hum_icon_tag = "<img src=\"icons/rel_hum.png\" width=\"" + icon_size + "\" height=\"" + icon_size + "\">";
var spo2_icon_tag = "<img src=\"icons/spo2.png\" width=\"" + icon_size + "\" height=\"" + icon_size + "\">";
var eco2_icon_tag = "<img src=\"icons/eco2.png\" width=\"" + icon_size + "\" height=\"" + icon_size + "\">";
var etvoc_icon_tag = "<img src=\"icons/etvoc.png\" width=\"" + icon_size + "\" height=\"" + icon_size + "\">";

var weather_icon_tag = "<img src=\"icons/weather.png\" width=\"" + icon_size + "\" height=\"" + icon_size + "\">";
var weather_forecast_icon_tag = "<img src=\"icons/weather_forecast.png\" width=\"" + icon_size + "\" height=\"" + icon_size + "\">";

var c_array = [];
var tv_array = [];
var t_array = [];
var h_array = [];
var p_array = [];
var lat_array = [];
var lng_array = [];
var coords_array = [];

window.onload = function () {
    document.getElementById('my-form').onsubmit = function () {
        /* do what you want with the form */

        console.log("Form is captured!");

        var retval = dateSelectChange();
        // You must return false to prevent the default form behavior
        return false;
    }
}

//////// --------------- Init Google map ---------------
function initMap() {
    console.log("------ Hi there! -------")
    console.log("Thanks for reviewing my code. Have fun!")

    // TODO: Set current day by default to show data, but this isn't the right place to do it!
    // let currentDate = new Date();
    // let cDay = currentDate.getDate();
    // let cMonth = currentDate.getMonth() + 1;
    // let cYear = currentDate.getFullYear();
    // // console.log("<b>" + cDay + "/" + cMonth + "/" + cYear + "</b>");

    // // Display data from current day by default
    // window.from_date_sel = cYear + "-" + cMonth + "-" + cDay; // example: 2021-06-16
    // window.to_date_sel = cYear + "-" + cMonth + "-" + cDay; 

    google_map = new google.maps.Map(document.getElementById('map_container'), {
        // center: new google.maps.LatLng(-17.417266, -66.132659),
        center: new google.maps.LatLng(initial_main_latitude, initial_main_longitude),
        mapTypeId: 'satellite', //  'terrain'
        zoom: 18
    });

    heatmap = new google.maps.visualization.HeatmapLayer();

    // To get new bouds for Googlemaps
    google.maps.event.addListener(google_map, 'idle', function (ev) {
        bounds = google_map.getBounds();
        console.log(" type of bounds: " + (typeof bounds));
        console.log("New map bounds: " + bounds);
        north_east = bounds.getNorthEast(); // LatLng of the north-east corner
        south_west = bounds.getSouthWest(); // LatLng of the south-west corder
        console.log("north_east[0]: " + north_east.lat());

        bounds_dict = {
            ne_lat: bounds.getNorthEast().lat(),
            ne_lng: bounds.getNorthEast().lng(),
            sw_lat: bounds.getSouthWest().lat(),
            sw_lng: bounds.getSouthWest().lng()
        }

        // Create and send a synthetic "bounds_changed" event to trigger the function userIdChange()
        var elemId = document.getElementById("map_container");
        let event = new Event("bounds_changed", {
            bubbles: true // Dummy data just for reference
        });
        elemId.dispatchEvent(event);
    });

    // Attach "bounds_changed" event from "map_container" to userIdChange() function
    var elemId = document.getElementById("map_container");
    elemId.addEventListener("bounds_changed", function (e) {
        // console.log("e.target:", e.target);
        // console.log("e.data:", e.data);
        userIdChange(event);
    },
        false
    );

    // Create the initial InfoWindow.
    const myLatlng = { lat: initial_main_latitude, lng: initial_main_longitude };

    // Debug: infoWindow example
    // let infoWindow = new google.maps.InfoWindow({
    //     content: "Click the map to get Lat/Lng!",
    //     position: myLatlng,
    // });
    // infoWindow.open(google_map);

    // Configure the click listener.
    google_map.addListener("click", (mapsMouseEvent) => {
        // Close the current InfoWindow.
        infoWindow.close();

        // Create a new InfoWindow.
        infoWindow = new google.maps.InfoWindow({
            position: mapsMouseEvent.latLng,
        });
        infoWindow.setContent(
            JSON.stringify(mapsMouseEvent.latLng.toJSON(), null, 2)
        );
        infoWindow.open(google_map);
    });
    getCurrentWeather();
    getWeatherForecast();
    setInterval(getCurrentWeather, 3000);
    setInterval(getWeatherForecast, 5000);


} //////// END: --------------- Init Google map ---------------

function captureForm() {
    console.log("Form is captured!");
    // dateSelectChange();
    return false;
}

//////// --------------- Data selection was changed ---------------
// Triggered by: 'date_select_from', 'date_select_to', 'user_id', 'send_checkbox' and 'map_container' (when bounds are modified)
function userIdChange(e) {
    console.log("userIdChange event id: " + e.target.id);

    if (e.target.id == 'map_container' && window.from_date_sel == "" && window.to_date_sel == "") {
        // window.from_date_sel = e.target.value;
        // console.log("Selected From date: " + e.target.value);
        console.log("Bounds changed, but no dates selected!");
        return;
    }

    // --------------- For debugging
    // window.from_date_sel = '2021-05-11';
    // window.to_date_sel = '2021-06-01';

    // window.from_date_sel = '2021-06-15';
    // window.to_date_sel = '2021-06-16';

    window.from_date_sel = document.getElementById('date_select_from').value;
    window.to_date_sel = document.getElementById('date_select_to').value;
    user_id_sel = document.getElementById('user_id').value;
    console.log("window.from_date_sel: " + window.from_date_sel);
    console.log("window.to_date_sel: " + window.to_date_sel);
    console.log("Current user ID: " + user_id_sel);
    if (user_id_sel == "") {
        console.log("User ID is empty!! ");
    }

    // Check the user has selected both dates: from - to
    if (window.from_date_sel != "" && window.to_date_sel != "") {
        // Check the dates aren't backwards. Create JavaScript Date objects:
        const fd = new Date(window.from_date_sel);
        const td = new Date(window.to_date_sel);
        if (td.valueOf() >= fd.valueOf()) { // valueOf() converts the date into milliseconds since the epoch
            console.log("Dates are good!")
            console.log("New bouds to send: " + bounds);

            console.log("bounds_dict.ne_lat: " + bounds_dict.ne_lat);

            refreshMapCallback(window.from_date_sel, window.to_date_sel, user_id_sel, bounds);

            document.getElementById('date_form_msg').style.color = "Green";
            document.getElementById('date_form_msg').innerHTML = "<< Query sent! >>";
            setTimeout(function () {
                document.getElementById("send_checkbox").checked = false;
                document.getElementById('date_form_msg').style.removeProperty('color');
                document.getElementById('date_form_msg').innerHTML = "Make your query...";
            }, 1000);
        } else {
            console.log("Your dates are backwards!")
            document.getElementById('date_form_msg').style.color = "Red";
            document.getElementById('date_form_msg').innerHTML = "Your dates are backwards!";
            document.getElementById("send_checkbox").checked = false;
        }
    } else if (window.from_date_sel == "" || window.to_date_sel == "") {
        document.getElementById('date_form_msg').style.color = "Blue";
        document.getElementById('date_form_msg').innerHTML = "Pick both dates to make a query";
        setTimeout(function () {
            document.getElementById("send_checkbox").checked = false;
            document.getElementById('date_form_msg').style.removeProperty('color');
            document.getElementById('date_form_msg').innerHTML = "Make your query...";
        }, 1000);
    }
}//////// END: --------------- Data selection was changed ---------------

//////// -------- Refresh map  ---------------------------
function refreshMapCallback(from_date, to_date, user_id, bounds) {
    // var infoWindow = new google.maps.InfoWindow;
    console.log('refreshMapCallback timeout!');
    console.log("Refresh From date: " + from_date);
    console.log("Refresh To date: " + to_date);
    console.log("User ID: " + user_id);

    console.log("north_east: " + north_east);
    console.log("south_west: " + south_west);

    window.main_latitude = initial_main_latitude;
    window.main_longitude = initial_main_longitude;

    //// -------- POST method -----------
    var data = {};
    data.from_date = window.from_date_sel;
    data.to_date = window.to_date_sel;
    data.user_id = user_id_sel;

    data.ne_lat = bounds_dict.ne_lat;
    data.ne_lng = bounds_dict.ne_lng;
    data.sw_lat = bounds_dict.sw_lat;
    data.sw_lng = bounds_dict.sw_lng;

    $.ajax({
        type: "POST",
        // url: "/airstrategist/ajax_post.php",
        url: "readings/dump_readings_xml.php/",
        data: data,
    }).done(function (data) {
        console.log('server response: ', data);
        var xml = data;

        // console.log("xml : " + xml);
        if (xml == null) {
            console.log("xml ERROR: " + data.responseText);
            document.getElementById('date_form_msg').style.color = "Red";
            document.getElementById('date_form_msg').innerHTML = "The server says: " + data.responseText;

            setTimeout(function () {
                document.getElementById("send_checkbox").checked = false;
                document.getElementById('date_form_msg').style.removeProperty('color');
                document.getElementById('date_form_msg').innerHTML = "Make your query...";
            }, 5000);
        } else {
            refreshReadings(xml);
        }
    }); //// END: -------- POST method -----------
    ////// END: -------- Refresh fire fighter data ---------------------------

}
//////// END: -------- Refresh map  ---------------------------

function sensorDisplayChange() {
    window.sel_sensor_index = document.getElementById("heatmap_select").value;
    //   sensor_name = document.getElementById("heatmap_select").textContent;
    var sensorSelectDropdown = document.getElementById("heatmap_select");
    window.sel_sensor_name = sensorSelectDropdown.options[sensorSelectDropdown.selectedIndex].text;

    //   console.log('sel_sensor_index: ', window.sel_sensor_index)
    console.log('sensor_name: ', window.sel_sensor_name)
}

function showMarkersChange() {
    const cb = document.getElementById('show_markers_checkbox');
    window.show_markers = cb.checked;

    console.log('show_markers: ', window.show_markers)
}

function refreshReadings(xml) {
    var infoWindow = new google.maps.InfoWindow;
    // Put the following block into an 'else' clause
    console.log("Getting fighters...");
    var fighters = xml.documentElement.getElementsByTagName('fighter');
    console.log("fighters : " + fighters);

    // Delete all previous fire fighter markers
    fa_len = fighter_marker_array.length;
    for (i = 0; i < fa_len; i++) {
        var marker = fighter_marker_array[i];
        if (marker) {
            marker.setMap(null);
            marker = null;
        }
    }
    // Delete all fire fighter marker references
    fighter_marker_array = [];
    var fighter_index = 0;

    // Delete all previous reading circles
    ca_len = reading_circle_array.length;
    for (i = 0; i < ca_len; i++) {
        var circle = reading_circle_array[i];
        if (circle) {
            circle.setMap(null);
            circle = null;
        }
    }
    // Delete all fire fighter marker references
    reading_circle_array = [];
    var circle_index = 0;

    heatmap.setMap(null);
    heatmap_array = [];
    var heatmap_index = 0;

    c_array = [];
    tv_array = [];
    t_array = [];
    h_array = [];
    p_array = [];
    lat_array = [];
    lng_array = [];
    coords_array = [];

    // Draw markers for all firefighters
    Array.prototype.forEach.call(fighters, function (fighterElem) {
        var id_code = fighterElem.getAttribute('id_code');
        var name = fighterElem.getAttribute('name');
        var unix_time = fighterElem.getAttribute('unix_time');
        var status = fighterElem.getAttribute('status');
        // console.log("status: " + status);
        // var type = fighterElem.getAttribute('type');
        var type = 'fire_fighter';
        var latitude = parseFloat(fighterElem.getAttribute('latitude'));
        var longitude = parseFloat(fighterElem.getAttribute('longitude'));
        var sensors = fighterElem.getAttribute('sensors');
        // console.log("sensors: " + sensors);

        // Converting JSON-encoded string to JS object
        var obj = JSON.parse(sensors);
        // console.log("Sensor vals c: " + obj.c);

        c_array.push(obj.c / 10); // Scaled down by 10 just for visualization purposes
        tv_array.push(obj.tv / 10); // Scaled down by 10 just for visualization purposes
        t_array.push(obj.t);
        h_array.push(obj.h);
        p_array.push(obj.p);
        lat_array.push(latitude);
        lng_array.push(longitude);

        var point = new google.maps.LatLng(latitude, longitude);

        coords_array.push(point);

        var text = document.createElement('text');

        var infowincontent = document.createElement('div');
        var strong = document.createElement('strong');
        strong.textContent = "Name: " + name;
        infowincontent.appendChild(strong);
        infowincontent.appendChild(document.createElement('br'));

        var text = document.createElement('text');
        // Show data rounding decimals. Convert first from string to float
        text.textContent = "[ Status: " + status + '\n'
            + " ][ ID: " + id_code + "\n"
            + " ][ Lat: " + parseFloat(latitude).toFixed(5) + '\n'
            + " ][ Lon: " + parseFloat(longitude).toFixed(5) + " ]"
            ;
        infowincontent.appendChild(text);

        // var infowincontent = document.createElement('div');
        infowincontent.appendChild(document.createElement('br'));
        var strong = document.createElement('strong');
        strong.textContent = "Sensors: ";
        infowincontent.appendChild(strong);
        infowincontent.appendChild(document.createElement('br'));

        var sensors_text = document.createElement('text');
        // Show data rounding decimals. Convert first from string to float
        // console.log("infoWindow obj.c: ", obj.c)
        sensors_text.textContent = "[ eCO2: " + obj.c + '\n'
            + " ][ eTVOC: " + obj.tv + "\n"
            + " ][ °C: " + (obj.t) + '\n'
            // + " ][ °C2: " + obj.t2 + '\n'
            + " ][ RH%: " + obj.h + '\n'
            + " ][ BP: " + (obj.p) + " ]"
            ;
        infowincontent.appendChild(sensors_text);

        // infowincontent.appendChild(text);
        var icon = customLabel[type] || {};

        var icon_source = statusIcon[status] || {};
        // console.log('icon_source.url', icon_source.url)

        // Add created markers only if 'show_markers' is checked
        if (show_markers) {
            var fighter_marker = new google.maps.Marker({
                map: google_map,
                position: point,
                // icon: {url: "https://maps.google.com/mapfiles/ms/icons/green-dot.png",
                //         scaledSize: new google.maps.Size(50, 50), // scaled size
                //       },
                // icon: {url: icon_source.url,
                //         scaledSize: new google.maps.Size(50, 50), // scaled size
                //       },
                label: icon.label
            });
    
            fighter_marker.addListener('click', function () {
                infoWindow.setContent(infowincontent);
                infoWindow.open(google_map, fighter_marker);
            });
    
            fighter_marker_array[fighter_index] = fighter_marker;
            fighter_index = fighter_index + 1;
        }
        
        // /////// heatmap -----------------------
        max_reading_weight = 100;

        var eco2_min = 300;
        var eco2_max = 2500;

        var amb_temp_min = 0;
        var amb_temp_max = 60;

        var rel_hum_min = 0;
        var rel_hum_max = 100;

        var bar_press_min = 0; //KPa
        var bar_press_max = 114;

        var etvoc_min = 0; 
        var etvoc_max = 400;

        // Compute normalized weights for each sensor reading type for comparative visualization
        // with the heatmap layer
        if (sel_sensor_name == 'amb_temp') {
            var amb_temp_reading = parseInt(obj.t);
            if (amb_temp_reading > amb_temp_max) { amb_temp_reading = amb_temp_max; }
            else if (amb_temp_reading < amb_temp_min) { amb_temp_reading = amb_temp_min; }

            var normalized_amb_temp_reading = amb_temp_reading / (amb_temp_max - amb_temp_min);
            reading_weight = normalized_amb_temp_reading * max_reading_weight;
        }

        if (sel_sensor_name == 'rel_hum') {
            var rel_hum_reading = parseInt(obj.h);
            if (rel_hum_reading > rel_hum_max) { rel_hum_reading = rel_hum_max; }
            else if (rel_hum_reading < rel_hum_min) { rel_hum_reading = rel_hum_min; }

            var normalized_rel_hum_reading = rel_hum_reading / (rel_hum_max - rel_hum_min);
            reading_weight = normalized_rel_hum_reading * max_reading_weight;
        }

        if (sel_sensor_name == 'bar_press') {
            var bar_press_reading = parseInt(obj.p);
            if (bar_press_reading > bar_press_max) { bar_press_reading = bar_press_max; }
            else if (bar_press_reading < bar_press_min) { bar_press_reading = bar_press_min; }

            var normalized_bar_press_reading = bar_press_reading / (bar_press_max - bar_press_min);
            reading_weight = normalized_bar_press_reading * max_reading_weight;
        }

        if (sel_sensor_name == 'eco2') {

            var eco2_reading = parseInt(obj.c);
            if (eco2_reading > eco2_max) { eco2_reading = eco2_max; }
            else if (eco2_reading < eco2_min) { eco2_reading = eco2_min; }

            var normalized_eco2_reading = eco2_reading / (eco2_max - eco2_min);
            reading_weight = normalized_eco2_reading * max_reading_weight;
        }

        if (sel_sensor_name == 'etvoc') {

            var etvoc_reading = parseInt(obj.tv);
            if (etvoc_reading > etvoc_max) { etvoc_reading = etvoc_max; }
            else if (etvoc_reading < etvoc_min) { etvoc_reading = etvoc_min; }

            var normalized_etvoc_reading = etvoc_reading / (etvoc_max - etvoc_min);
            reading_weight = normalized_etvoc_reading * max_reading_weight;
        }

        //// ------ Heatmap layer --------
        // console.log("reading_weight = " + reading_weight);

        heatmap_point = { location: point, weight: reading_weight };
        heatmap_array[heatmap_index] = heatmap_point;

        heatmap_index = heatmap_index + 1;
        // refresh2dGraph(xml); // This should be outside this 'forEach'?
    });

    refresh2dGraph();

    refresh3dGraph();

    // console.log("fighter_marker_array: " + fighter_marker_array);
    // console.log("heatmap_array: " + heatmap_array);
    heatmap = new google.maps.visualization.HeatmapLayer({
        data: heatmap_array,
        dissipating: true, // default value is true
        // maxIntensity: 10,
        // opacity: 1.0,
        radius: 70,
        radius: 40,
    });
    heatmap.setMap(google_map);
}

////// -------- Update 3D sensor graph  ---------------------------
function refresh3dGraph() {

    // Find min. latitude (y axis) and min. longitude (x axis)
    min_lat = null;
    min_lng = null;

    polar_cartesian_coords = [];

    coords_array.forEach(findMinLatLng);

    function findMinLatLng(item, index) {
        // console.log("item.lat(): ", item.lat())
        // Find the minimum latitude in the visible screen space
        if (min_lat == null || item.lat() < min_lat) {
            min_lat = item.lat();
        }
        // Find the minimum longitude in the visible screen space
        if (min_lng == null || item.lng() < min_lng) {
            min_lng = item.lng();
        }
    }

    // console.log("min_lat: ", min_lat);
    // console.log("min_lng: ", min_lng);

    var origin_coord = new google.maps.LatLng(min_lat, min_lng);
    coords_array.forEach(findHaversineDist);
    function findHaversineDist(item, index) {

        var distance = haversineDistance(origin_coord, item);
        var azimuth = forwardAzimuth(origin_coord, item);

        // Convert from bearing to cartesian angle
        var alpha = (5 / 2) * Math.PI - azimuth; // (5/2)*Math.PI => 450 degrees

        if (alpha > 2 * Math.PI) { // If more than 360 degrees
            alpha = alpha - 2 * Math.PI;
        }

        // Compute Cartesian coordinates
        x_coord = distance * Math.cos(alpha);
        y_coord = distance * Math.sin(alpha);

        // Store polar and Cartesian coordinates
        polar_cartesian_coords.push({ r: distance, phi: azimuth, x: x_coord, y: y_coord });

        // console.log("distance: ", distance)
        // console.log("azimuth: ", azimuth)
        // console.log("x_coord: ", x_coord)
        // console.log("y_coord: ", y_coord)
    }

    // Create Scattered type 3D graph:
    x_coords = []; y_coords = [];

    for (i = 0; i < c_array.length; i++) {
        var x1_ = polar_cartesian_coords[i].x;
        // var x1_ = Math.random();
        x_coords.push(x1_);

        var y1_ = polar_cartesian_coords[i].y;
        // var y1_ = Math.random();
        y_coords.push(y1_);
    }
    var c_trace = {
        // x: x_coords, y: y_coords, z: c_array,
        x: x_coords, y: y_coords, z: c_array,
        mode: 'markers',
        marker: {
            size: 12,
            line: {
                color: 'rgba(217, 217, 217, 0.14)',
                width: 0.5
            },
            // showscale: true,
            color: c_array,
            colorscale: [[0, 'rgb(0,0,255)'], [1, 'rgb(255,0,0)']],
            // colorscale: [[0, 'rgb(0,0,255)'], [1, 'rgb(255,67,5)']],
            
            // colorscale: c_array.colorscale,
            opacity: 0.8
        },
        type: 'scatter3d',
        name: 'eCO2 [PPM/10]'
    };

    var tv_trace = {
        x: x_coords, y: y_coords, z: tv_array,
        mode: 'markers', //'markers',
        marker: {
            // color: 'rgb(127, 127, 127)',
            size: 12,
            symbol: 'circle', // "circle" | "circle-open" | "square" | "square-open" | "diamond" | "diamond-open" | "cross" | "x" 
            line: {
                // color: 'rgb(204, 204, 204)',
                width: 1
            },
            color: tv_array,
            // colorscale: [[0, 'rgb(255,0,0)'], [1, 'rgb(0,255,0)']],
            colorscale: [[0, 'rgb(0,255,0)'], [1, 'rgb(255,255,0)']],
            opacity: 0.8
        },
        type: 'scatter3d',
        name: 'eTVOC [PPB/10]'
    };

    var t_trace = {
        x: x_coords, y: y_coords, z: t_array,
        mode: 'markers', //'markers',
        marker: {
            size: 12,
            symbol: 'circle',
            line: {
                width: 1
            },
            // color: t_array,
            colorscale: [[0, 'rgb(0, 255,0)'], [1, 'rgb(0, 0,255)']],
            opacity: 0.8
        },
        type: 'scatter3d',
        name: 'Amb. Temp.[°C]'
    };

    var h_trace = {
        x: x_coords, y: y_coords, z: h_array,
        mode: 'markers', //'markers',
        marker: {
            size: 12,
            symbol: 'circle',
            line: {
                width: 1
            },
            // color: h_array,
            colorscale: [[0, 'rgb(255,0,0)'], [1, 'rgb(0, 0,255)']],
            opacity: 0.8
        },
        type: 'scatter3d',
        name: 'RH [%]'
    };

    var p_trace = {
        x: x_coords, y: y_coords, z: p_array,
        
        mode: 'markers', //'markers',
        marker: {
            size: 12,
            symbol: 'circle',
            line: {
                width: 1
            },
            // color: p_array,
            colorscale: [[0, 'rgb(0,255,0)'], [1, 'rgb(255,0, 0)']],
            opacity: 0.8
        },
        type: 'scatter3d',
        name: 'Bar. Press. [KPa]'
    };

    // var data = [c_trace, tv_trace];
    // [p_graph, c_graph, tv_graph, t_graph, h_graph];
    var data = [p_trace, c_trace, tv_trace, t_trace, h_trace,];
    var layout = {
        margin: {
            l: 0,
            r: 0,
            b: 0,
            t: 0
        },
        title: {
            text: '<b>Sensor Data vs. Position</b>',
            font: {
                family: 'Courier New, monospace',
                size: 20
            },
            xref: 'paper',
            x: 0.00,
        }
    };
    Plotly.newPlot('graph_3d', data, layout);

}

////// -------- Update 2D sensor graph  ---------------------------
function refresh2dGraph() {

    // console.log('Updating 2D graph');

    // TODO: play with the 'fill' type parameter for better visualization
    var c_graph = {
        y: c_array,
        type: 'line',
        // fill: 'tonexty',
        name: 'eCO2 [PPM/10]'
    };

    var tv_graph = {
        y: tv_array,
        type: 'line',
        fill: 'tonexty',
        name: 'eTVOC [PPB/10]'
    };

    var t_graph = {
        y: t_array,
        type: 'line',
        fill: 'tonexty', //fill: 'tozeroy',
        name: 'Ambient [°C]'
    };

    var h_graph = {
        y: h_array,
        type: 'line',
        // fill: 'tonexty',
        name: 'RH [%]'
    };

    var p_graph = {
        y: p_array,
        type: 'line',
        // fill: 'tonexty',
        name: 'Bar. Press. [KPa]'
    };

    // Order of the curves: first graphs in the array are drawn way in the back
    var data = [p_graph, c_graph, tv_graph, t_graph, h_graph];

    layout = {
        // autosize: false,
        // height: 50,
        margin: {
            l: 40,
            r: 80,
            b: 40,
            t: 40,
            pad: 4
        },

        xaxis: {
            title: {
                text: 'Sensor Measurements',
                font: {
                    family: 'Courier New, monospace',
                    size: 18,
                    color: '#7f7f7f'
                }
            },
            // scaleanchor: "y"
        },
        yaxis: {
            // scaleanchor: "x",
            title: {
                //   text: 'Num/%',
                font: {
                    family: 'Courier New, monospace',
                    size: 18,
                    color: '#7f7f7f'
                }
            },
        },
        title: {
            text: '<b>Sensor Data vs. Time</b>',
            font: {
                family: 'Courier New, monospace',
                size: 20
            },
            xref: 'paper',
            x: 0.00,
        },
        plot_bgcolor: "#393d3f", // https://coolors.co/393d3f-fdfdff-c6c5b9-62929e-546a7b
        paper_bgcolor: "#fdfdff"
    };

    var config = { responsive: true }

    Plotly.react('graph_2d', data, layout, config);
}; ////// END: -------- Update sensor graph  ---------------------------

function toRad(x) {
    return x * Math.PI / 180;
}

// Computes the Haversine distance in Km between a pair of GPS coordinates
function haversineDistance(coords1, coords2) {

    var dLat = toRad(coords2.lat() - coords1.lat());
    var dLon = toRad(coords2.lng() - coords1.lng())

    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(toRad(coords1.lat())) *
        Math.cos(toRad(coords2.lat())) *
        Math.sin(dLon / 2) * Math.sin(dLon / 2);

    return 12742 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)); // distance in Km
}

// Computes the Azimuth angle in radians between a pair of GPS coordinates
function forwardAzimuth(cur_point, goal_point) {
    var gps_f_lat_rad = toRad(cur_point.lat());
    var gps_f_lon_rad = toRad(cur_point.lng());
    var waypoint_lon_rad = toRad(goal_point.lng());
    var waypoint_lat_rad = toRad(goal_point.lat());

    waypoint_angle = Math.atan2(
        Math.sin(waypoint_lon_rad - gps_f_lon_rad) * Math.cos(waypoint_lat_rad),
        Math.cos(gps_f_lat_rad) * Math.sin(waypoint_lat_rad) - Math.sin(gps_f_lat_rad)
        * Math.cos(waypoint_lat_rad) * Math.cos(waypoint_lon_rad - gps_f_lon_rad));

    return waypoint_angle; // radians
}

function downloadUrl2(url, callback) {
    var data = {};
    data.from_date = window.from_date_sel;
    data.to_date = window.to_date_sel;
    // data.bounds = bounds;

    $.ajax({
        type: "POST",
        url: url,
        data: data,
    }).done(function (data) {
        console.log('server response: ', data);
        return data;
    });
}

function downloadUrl(url, callback) {
    var request = window.ActiveXObject ?
        new ActiveXObject('Microsoft.XMLHTTP') :
        new XMLHttpRequest;

    request.onreadystatechange = function () {
        if (request.readyState == 4) {
            request.onreadystatechange = doNothing;
            callback(request, request.status);
        }
    };

    request.open('GET', url, true);
    request.send(null);
}


////// -------- Get current weather ---------------------------
// setInterval(getCurrentWeather, 3000);
function getCurrentWeather() {
    if (window.main_latitude != null && window.main_longitude != null) {
        link = "https://api.openweathermap.org/data/2.5/weather?lat=" + window.main_latitude
            + "&lon=" + window.main_longitude + "&units=metric&apikey=<put-your-api-key-here>";

        var request = new XMLHttpRequest();

        request.open('GET', link, true);

        request.onload = function () {
            var obj = JSON.parse(this.response);
            if (request.status >= 200 && request.status < 400) {
                // var temp = obj.main.temp;
                var temp = obj.main.temp;
                var humidity = obj.main.humidity;
                var weather_main = obj.weather[0].main;
                var weather_description = obj.weather[0].description;
                var wind_speed = obj.wind.speed;
                var wind_direction = obj.wind.deg;
                var cloudiness = obj.clouds.all;


                // console.log("CURRENT WEATHER: You have it!", temp);
                document.getElementById('current_weather').innerHTML = "<strong>" + weather_icon_tag + "Current Weather:</strong><br>"
                    + 'Temp: ' + temp + "°C | RH: " + humidity
                    + "% | Weather: " + weather_main + ", " + weather_description
                    + " | Wind: " + wind_speed + "m/s, " + wind_direction + "° | Clouds: " + cloudiness + "%";
            }
            else {
                console.log("CURRENT WEATHER: That didn't work...");
            }
        }
        request.send();
    }
}
////// END: -------- Get current weather ---------------------------

////// -------- Get the weather forecast ---------------------------
// setInterval(getWeatherForecast, 5000);
function getWeatherForecast() {
    if (window.main_latitude != null && window.main_longitude != null) {
        link = "https://api.openweathermap.org/data/2.5/forecast?lat=" + window.main_latitude
            + "&lon=" + window.main_longitude + "&units=metric&apikey=<put-your-api-key-here>";

        var request = new XMLHttpRequest();

        request.open('GET', link, true);

        request.onload = function () {
            var obj = JSON.parse(this.response);

            var num_forecasts = 8;

            var weather_forecast_html = "<strong>" + weather_forecast_icon_tag + "Three-hour Forecasts:</strong>";
            if (request.status >= 200 && request.status < 400) {
                for (var idx = 0; idx < num_forecasts; ++idx) {
                    var temp = obj.list[idx].main.temp;
                    var humidity = obj.list[idx].main.humidity;
                    var weather_main = obj.list[idx].weather[0].main;
                    var weather_description = obj.list[idx].weather[0].description;
                    var wind_speed = obj.list[idx].wind.speed;
                    var wind_direction = obj.list[idx].wind.deg;
                    var cloudiness = obj.list[idx].clouds.all;

                    // Sometimes the following element is absent. Defaulting to zero:
                    var rain_3hr = "0";
                    if (obj.list[idx].rain && obj.list[idx].rain['3h']) {
                        var rain_3hr = obj.list[idx].rain['3h'];
                    }

                    weather_forecast_html = weather_forecast_html + '<br><strong>+' + (idx + 1) * 3 + 'Hrs></strong> Temp: ' + temp + "°C | RH: " + humidity
                        + "% | Weather: " + weather_main + ", " + weather_description
                        + " | Rain: " + rain_3hr
                        + "mm | Wind: " + wind_speed + "m/s, " + wind_direction + "° | Clouds: " + cloudiness + "%";
                }
                // console.log("WEATHER FORECAST: You have it!", temp);

                document.getElementById('weather_forecast').innerHTML = weather_forecast_html;

            }
            else {
                console.log("WEATHER FORECAST: That didn't work...");
            }
        }
        request.send();
    }
}
////// END: -------- Get the weather forecast ---------------------------

// Someday I will be doing something!
function doNothing() { }


