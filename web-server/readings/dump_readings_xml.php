<?php
	// Default time zone
	date_default_timezone_set("America/La_Paz");

	$flag = False;
	$user_id = "";
	// echo "Heres your answer!\n";

	if (isset($_POST['from_date']) && isset($_POST['to_date'])) {
		$from_date =   $_POST["from_date"];
		$to_date =   $_POST["to_date"];
		
		if (isset($_POST['user_id'])) {
			$user_id = $_POST['user_id'];
		}

		if (isset($_POST['ne_lat']) && isset($_POST['ne_lng']) && isset($_POST['sw_lat']) && isset($_POST['sw_lng'])) {
			$ne_lat = $_POST['ne_lat'];
			$ne_lng = $_POST['ne_lng'];
			$sw_lat = $_POST['sw_lat'];
			$sw_lng = $_POST['sw_lng'];
			// echo "ne_lat: $ne_lat, ne_lng: $ne_lng, sw_lat: $sw_lat, sw_lng: $sw_lng";
		}

		// echo $from_date . "-" . $to_date . "</br>";

		$flag = True;

		// Conectarse a la base de datos
	    require_once 'login.php'; // Inclusion de script con datos de 
		
		// Create connection
		$conn = new mysqli($hn, $un, $pw, $db);
		// Check connection
		if ($conn->connect_error) {
		  die("Connection failed: " . $conn->connect_error);
		}

		// For debugging:
		// $startDateTime = "2021-06-16 00:00:00";
		// $endDateTime = "2021-06-17 23:59:59";

		$startDateTime = $from_date . " 00:00:00";
		$endDateTime = $to_date . " 23:59:59";


		dumpXmlFromDb($conn, $startDateTime, $endDateTime, $user_id, (float)$ne_lat, $ne_lng, $sw_lat, $sw_lng);


		$conn->close();
    }

	function dumpXmlFromDb($conn, $startDateTime, $endDateTime, $user_id, $ne_lat, $ne_lng, $sw_lat, $sw_lng) {

		$sql = "SELECT * FROM aq_readings WHERE ";
		if ($user_id != "") {
			$sql = $sql . "id_code='$user_id' AND ";
		} 

		$sql = $sql . "latitude BETWEEN $sw_lat AND $ne_lat 
			AND longitude BETWEEN $sw_lng AND $ne_lng 
			AND unix_time BETWEEN '$startDateTime' AND '$endDateTime'";
	
		$result = $conn->query($sql);

		if ($result->num_rows > 0) {

			header("Content-type: text/xml");

			// Start XML file, echo parent node
			echo "<?xml version='1.0' ?>";
			echo '<fighters>';

			// output data of each row
			while($row = $result->fetch_assoc()) {
				// echo "latitude: " . $row["latitude"]. " - longitude: " . $row["longitude"]. " " . $row["sensors"]. "<br>";

	            // Replace double quotes in the 'sensors' JSON string with ' &quot;' to include the string as valid XML data
	            $replaced = str_replace("\"", "&quot;", $row["sensors"]);

				// Add to XML document node
				echo '<fighter ';
				// echo 'unix_time="' . $row["unix_time"] . '" ';
				echo 'unix_time="' . '1620760913' . '" ';
				echo 'name="' . $row["name"] . '" ';
				echo 'id_code="' . $row["id_code"] . '" ';
				echo 'latitude="' . $row["latitude"] . '" ';
				echo 'longitude="' . $row["longitude"] . '" ';
				// echo 'status="' . $row["status"] . '"'; 
				echo 'status="' . $row["status"] . '" ';
				echo 'sensors="' . $replaced . '"';
				echo '/>';
			}
			// End XML file
			echo '</fighters>';
		} else {
		  echo "0 results";
		}
	}

?>