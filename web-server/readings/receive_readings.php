<?php
    $DEBUG = False;

    // Default time zone
	date_default_timezone_set("America/La_Paz");

    // Conectarse a la base de datos
    require_once 'login.php'; // Inclusion de script con datos de login
    // echo "Login required";
    $conexion = new mysqli($hn, $un, $pw, $db);
    if ($conexion->connect_error) die($conexion->connect_error);
    echo "Connected to DB!";

	// Send a message to the client
    $nombre_servidor = gethostname();
    $ip_servidor = $_SERVER['SERVER_ADDR'];  //getHostByName(getHostName());
    echo "Received POST data at: " . $nombre_servidor . " " . $ip_servidor . " !\n"; // Just for debugging
	
	//// --------- Store data from received HTTP POST in text file ------------------------------
	if (isset($_POST['unix_time']) && isset($_POST['name']) && isset($_POST['id_code']) &&
      isset($_POST['latitude']) && isset($_POST['longitude']) && isset($_POST['status']) && isset($_POST['sensors'])) {
	    // 
    	$unix_time =   $_POST["unix_time"];
    	$name =   $_POST["name"];
    	$id_code =   $_POST["id_code"];
    	$latitude =   $_POST["latitude"];
    	$longitude =   $_POST["longitude"];
    	$status =   $_POST["status"];
        $sensors =   $_POST["sensors"];
        echo "Received sensors: " . $sensors . "\n";

        // WRNING: Just for debugging. This creates fake GPSdata dispersion!
        if ($DEBUG == True) {
            $rlat = rand(-100,100) / 100000.0;
            $latitude = $latitude + $rlat;
            $rlon = rand(-100,100) / 100000.0;
            $longitude = $longitude + $rlon;
        }
    	
    	// Make data string to store in text file
    	$data =  date('D d M Y H:i:s', $unix_time) . ',' . $unix_time . ',' . $name . ',' . $id_code . ',' . $latitude . ','  . $longitude . ',' . $status. ',' . $sensors;
    	
        // 2021-05-12 15:21:53
        $formatted_date = date('Y-m-d H:i:s', $unix_time);
        echo "formatted_date: $formatted_date \n";

    	// Store data in a text file
        $file =  './data/'. str_replace(' ', '_', $name) . '.txt';
    	// file_put_contents($file, $data, FILE_APPEND | LOCK_EX);
    	file_put_contents($file, $data, LOCK_EX);

        // Construir cadena de texto para la consulta a la base de datos
        $consulta    = "INSERT INTO aq_readings(unix_time, name, id_code, latitude, longitude, status, sensors)
 VALUES('$formatted_date', '$name', '$id_code', '$latitude', '$longitude', '$status', '$sensors')";

        // Enviar consulta a la base de datos
        $resultado   = $conexion->query($consulta);

        // Si la consulta fallo, imprimir mensaje en la pagina
        if (!$resultado) echo "INSERT fallo: $consulta<br>" .
          $conexion->error . "<br><br>";
        else echo "Inserted in DB!\r\n";
	}
	else {
	    // Send a message to the client
	    echo "One or more data pairs weren't received...\n"; // Just for debugging
	}
?>
